-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 28, 2024 at 08:26 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coba5`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_siswa`
--

CREATE TABLE `tbl_siswa` (
  `id_siswa` int(11) NOT NULL,
  `nis` varchar(5) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_siswa`
--

INSERT INTO `tbl_siswa` (`id_siswa`, `nis`, `nama`, `kelas`, `alamat`) VALUES
(81, '11113', 'Pan1', 'kelas1', 'Mana aja'),
(82, '11114', 'Pan2', 'kelas2', 'Mana aja'),
(83, '11115', 'Pan3', 'kelas3', 'Mana aja'),
(84, '11116', 'Pan4', 'kelas4', 'Mana aja'),
(85, '11117', 'Pan5', 'kelas5', 'Mana aja'),
(86, '11118', 'Pan6', 'kelas6', 'Mana aja'),
(87, '11119', 'Pan7', 'kelas7', 'Mana aja'),
(88, '11120', 'Pan8', 'kelas8', 'Mana aja'),
(89, '11121', 'Pan9', 'kelas9', 'Mana aja'),
(90, '11122', 'Pan10', 'kelas10', 'Mana aja'),
(91, '11123', 'Pan11', 'kelas11', 'Mana aja'),
(92, '11124', 'Pan12', 'kelas12', 'Mana aja'),
(93, '11125', 'Pan13', 'kelas13', 'Mana aja'),
(94, '11126', 'Pan14', 'kelas14', 'Mana aja'),
(95, '11127', 'Pan15', 'kelas15', 'Mana aja'),
(96, '11128', 'Pan16', 'kelas16', 'Mana aja'),
(97, '11129', 'Pan17', 'kelas17', 'Mana aja'),
(98, '11130', 'Pan18', 'kelas18', 'Mana aja'),
(99, '11131', 'Pan19', 'kelas19', 'Mana aja'),
(100, '11132', 'Pan20', 'kelas20', 'Mana aja');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
