<?php

namespace App\Controllers;

use App\Models\ModelSiswa;

class Home extends BaseController
{
    public function __construct() {
        helper('form');
        $this->ModelSiswa = new ModelSiswa();
    }
    
    public function index()
    {
        $data = array(
            'siswa' => $this->ModelSiswa->alldata(),
        );
        return view('v_siswa', $data);
    }

    public function import()
    {
        $file = $this->request->getFile('file_excel');
        $ext = $file->getClientExtension();

        if ($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        $spreadsheet = $render->load($file);
        $sheet = $spreadsheet->getActiveSheet()->toArray();

        foreach ($sheet as $x => $excel) {
            // skip baris pertama
            if ($x == 0) {
                continue;
            }

            $data = [
                'nis' => $excel['1'],
                'nama' => $excel['2'],
                'kelas' => $excel['3'],
                'alamat' => $excel['4'],
            ];
            // skip jika data duplikat
            $nis = $this->ModelSiswa->cekdata($excel['1']);
            if ($excel['1'] == $nis['nis']) {
                continue;
            }

            $this->ModelSiswa->add($data);
        }

        session()->setFlashdata('pesan', 'Data berhasil diimport!');
        return redirect()->to(base_url('Home'));
    }
}
