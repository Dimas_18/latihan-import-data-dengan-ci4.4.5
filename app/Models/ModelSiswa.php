<?php

namespace App\Models;

use CodeIgniter\Model;

class ModelSiswa extends Model
{
    public function alldata() {
        return $this->db->table('tbl_siswa')->get()->getResultArray();
    }

    public function cekdata($nis) {
        return $this->db->table('tbl_siswa')
        ->where('nis', $nis)
        ->get()->getRowArray();
    }

    public function add($data) {
        $this->db->table('tbl_siswa')->insert($data);
    }
}
